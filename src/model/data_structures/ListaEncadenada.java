package model.data_structures;


import java.util.Iterator;
import java.util.NoSuchElementException;

public class ListaEncadenada<T>  implements IListaEncadenada <T>
{



	/**
	 *  Constante de serialización
	 */
	private static final long serialVersionUID = 1L;

	private Nodo<T> primerNodo;

	private int cantidadElementos;

	private Nodo<T> actual;

	/**
	 * Construye una lista vacia
	 * <b>post:< /b> se ha inicializado el primer nodo en null
	 */
	public ListaEncadenada() 
	{
		primerNodo = null;
		cantidadElementos = 0;
		actual = primerNodo;
	}

	/**
	 * Se construye una nueva lista cuyo primer nodo  guardará al elemento que llega por parámentro
	 * @param nPrimero el elemento a guardar en el primer nodo
	 * @throws NullPointerException si el elemento recibido es nulo
	 */
	public ListaEncadenada(T nPrimero)
	{
		if(primerNodo == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}
		primerNodo = new Nodo<T>(nPrimero);
		cantidadElementos = 1;
		actual = primerNodo;
	}


	@Override
	public Iterator<T> iterator() 
	{
		// TODO Auto-generated method stub
		return new IteratorLista<T>(primerNodo);
	}

	@Override
	public Nodo<T> darNodo(int pos) 
	{
		Nodo<T> actual = primerNodo;

		if(pos < 0 || pos > cantidadElementos)
		{
			throw new IndexOutOfBoundsException("Se está pidiendo el indice: " + pos + " y el tamaño de la lista es de " + cantidadElementos);
		}

		else
		{
			int posActual = 0;

			while(actual != null && posActual < pos)
			{
				actual = actual.darSiguiente();
				posActual ++;
			}
		}
		return actual;
		// TODO Auto-generated method stub


	}

	

	
	public void add(T elem) 
	{
		// TODO Auto-generated method stub

		if(elem == null)
		{
			throw new NullPointerException("El elemento para agregar no existe");
		}

		else
		{
			if(primerNodo == null)
			{
				primerNodo =  new Nodo<T>(elem);
				actual=primerNodo;
				cantidadElementos++;
			}

			else
			{
				Nodo<T> nuevo = new Nodo<T>(elem);
				nuevo.cambiarSiguiente(primerNodo);
				primerNodo = nuevo;
				actual=primerNodo;
				cantidadElementos++;
			}


		}
		
		
	}
	public boolean contains(Object o) 
	{
		// TODO Completar según la documentación
		boolean tiene = false;

		if(primerNodo != null )
		{
			Nodo<T> actual = primerNodo;

			for(int i = 0  ; i < cantidadElementos && !tiene ; i ++)
			{

				if(actual.darElemento().equals(o))
				{
					tiene = true;

				}
				else if(actual.equals(o))
				{
					tiene = true;
				}

				actual = actual.darSiguiente();

			}
		}
		return tiene;
	}




	@Override
	public T getElement(int pos) 
	{
		// TODO Auto-generated method stub
		Nodo<T> actual = primerNodo;

		if(pos < 0 || pos > cantidadElementos)
		{
			throw new IndexOutOfBoundsException("Se está pidiendo el indice: " + pos + " y el tamaño de la lista es de " + cantidadElementos);
		}

		else
		{
			int posActual = 0;

			while(actual != null && posActual < pos)
			{
				actual = actual.darSiguiente();
				posActual ++;
			}
		}
		return (T) actual;
	}

	@Override
	public T getCurrentElement() {
		// TODO Auto-generated method stub
		return (T) actual;
	}

	@Override
	public Integer getSize() {
		return cantidadElementos;
	}

	@Override
	public boolean delete(T elem) {

		// TODO Auto-generated method stub
		boolean elimino = false;

		if((Nodo<T>)primerNodo != null && contains(elem))
		{
			if(primerNodo.darElemento().equals(elem))
			{
				primerNodo = (Nodo<T>)primerNodo.darSiguiente();
				elimino = true;
				cantidadElementos--;
			}
			else if(primerNodo.equals(elem))
			{
				primerNodo = (Nodo<T>)primerNodo.darSiguiente();
				elimino = true;
				cantidadElementos--;
			}
			else if(getElement(cantidadElementos-1).equals(elem))
			{
				darNodo(cantidadElementos-2).cambiarSiguiente(null);
				elimino = true;
				cantidadElementos--;
			}
			else
			{
				Nodo<T> siguientes = (Nodo<T>) darNodo(cantidadElementos);
				Nodo<T> anterior = (Nodo<T>)primerNodo;
				int i = 0;
				while(i < cantidadElementos-1 && !elimino)
				{
					anterior = (Nodo<T>) anterior.darSiguiente();
					i++;
				}

				anterior.cambiarSiguiente(siguientes);
				siguientes.cambiarAnterior(anterior);
				cantidadElementos --;
				elimino = true;
			}
		}
		return elimino;

	}



	@Override
	public Nodo<T> next() 
	{

		return actual.darSiguiente();
	}

	@Override
	public Nodo<T> previous() {
		// TODO Auto-generated method stub
		return actual.darAnterior();
	}


}






