package model.data_structures;

public interface IListaEncadenada <T>extends Iterable<T>
{
	


Nodo<T> darNodo(int pos);

	
	void add(T elem);
	
	
	T getElement(int pos);
	
	
	T getCurrentElement();
	
	
	Integer getSize();
	
	
	boolean delete(T elem);
	
	
	Nodo<T> next();
	
	
	Nodo<T> previous();
}
