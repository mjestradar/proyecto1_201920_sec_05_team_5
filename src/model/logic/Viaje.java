package model.logic;

public class Viaje implements Comparable <Viaje>
{
	
	
	private int sourceId;
	
	private int dstId;
	
	private int hod;
	
	private double meanTravelTime;
	
	private double standardDeviationTravelTime;
	
	private double geometricMeanTravelTime;
	
	private double geometricStandardDeviationTravelTime;
	
	
	public Viaje(int a, int b, int c, double d, double e, double f, double g) {
		sourceId=a;
		dstId=b;
		hod=c;
		meanTravelTime=d;
		standardDeviationTravelTime=e;
		geometricMeanTravelTime=f;
		geometricStandardDeviationTravelTime=g;
	}


	public int getSourceId() {
		return sourceId;
	}


	public int getDstId() {
		return dstId;
	}


	public int getHod() {
		return hod;
	}


	public double getMeanTravelTime() {
		return meanTravelTime;
	}


	public double getStandardDeviationTravelTime() {
		return standardDeviationTravelTime;
	}


	public double getGeometricMeanTravelTime() {
		return geometricMeanTravelTime;
	}


	public double getGeometricStandardDeviationTravelTime() {
		return geometricStandardDeviationTravelTime;
	}


	
	public int compareTo(Viaje pViaje) {
		
		return compareTo(pViaje);
	}
	
	
	
}
