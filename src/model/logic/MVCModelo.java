package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

import com.opencsv.CSVReader;


import model.data_structures.ListaEncadenada;
import model.data_structures.Queue;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo 
{
	/**
	 * Atributos del modelo del mundo
	 */


	private ListaEncadenada<Viaje> datos1;
	private ListaEncadenada<Viaje> datos2;
	private Queue <Viaje> datos3 ;
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		datos1 = new ListaEncadenada <Viaje>();
		datos3 = new Queue <Viaje>();
		datos2 = new ListaEncadenada <Viaje>();
	}



	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */


	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */ 
	public void agregar1(Viaje dato)
	{
		datos1.add(dato);
	}

	public void agregar2(Viaje dato)
	{
		datos2.add(dato);
	}
	public void agregar3(Viaje dato)
	{	

		datos3.enqueue(dato);
	}


	/**
	 * Requerimiento buscar dato
	 * @param dato Dato a buscar
	 * @return dato encontrado
	 */



	public int[] cargar(int trimestre)throws Exception {

		int n1 =0;
		int n2 =0;
		int n3=0;
		CSVReader reader2 = null;
		CSVReader reader1= null;
		CSVReader reader3=null;
		try 
		{
			if( trimestre>4)
			{
				throw new Exception("Trimestre inexistente.");
			}else {
				reader1 = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+trimestre+"-All-MonthlyAggregate.csv"));
				String[] nextLine1=reader1.readNext();
				while ((nextLine1 = reader1.readNext()) != null) 
				{
					Viaje elem1 = new Viaje(Integer.parseInt(nextLine1[0]), Integer.parseInt(nextLine1[1]), 
							Integer.parseInt(nextLine1[2]), Double.parseDouble(nextLine1[3]),
							Double.parseDouble(nextLine1[4]), Double.parseDouble(nextLine1[5]), 
							Double.parseDouble(nextLine1[6]));

					agregar1(elem1);

					n1++;

				}    
				reader2 = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+trimestre+"-All-WeeklylyAggregate.csv"));
				String[] nextLine2=reader2.readNext();
				while ((nextLine2 = reader2.readNext()) != null) 
				{
					Viaje elem = new Viaje(Integer.parseInt(nextLine2[0]), Integer.parseInt(nextLine2[1]), 
							Integer.parseInt(nextLine2[2]), Double.parseDouble(nextLine2[3]),
							Double.parseDouble(nextLine2[4]), Double.parseDouble(nextLine2[5]), 
							Double.parseDouble(nextLine2[6]));

					agregar2(elem);

					n2++;
				}


				reader3 = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+trimestre+"-All-HourlyAggregate.csv"));
				String[] nextLine3=reader3.readNext();
				while ((nextLine3 = reader3.readNext()) != null) 
				{
					Viaje elem = new Viaje(Integer.parseInt(nextLine3[0]), Integer.parseInt(nextLine3[1]), 
							Integer.parseInt(nextLine3[2]), Double.parseDouble(nextLine3[3]),
							Double.parseDouble(nextLine3[4]), Double.parseDouble(nextLine3[5]), 
							Double.parseDouble(nextLine3[6]));

					agregar3(elem);

					n3++;
				}
			}

		}

		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		finally
		{
			if (reader2 != null) 
			{
				try 
				{
					reader2.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		int []rta = {n1, n2, n3};
		return rta;
	}
	
	 public static void selectSort(Iterator<Viaje>[] a) {
	        int n = a.length;
	        for (int i = 0; i < n; i++) {
	            int min = i;
	            for (int j = i+1; j < n; j++) {
	                if (less(a[j], a[min])) min = j;
	            }
	            exch(a, i, min);
	            
	        }
	        
	    }

	    /**
	     * Rearranges the array in ascending order, using a comparator.
	     * @param a the array
	     * @param comparator the comparator specifying the order
	     */
	    public static void sort(Object[] a, Comparator<Viaje> comparator) {
	        int n = a.length;
	        for (int i = 0; i < n; i++) {
	            int min = i;
	            for (int j = i+1; j < n; j++) {
	                if (less(comparator, a[j], a[min])) min = j;
	            }
	            exch(a, i, min);    
	        }	        
	    }


	   /***************************************************************************
	    *  Metodos de ayuda para ordenar.
	    ***************************************************************************/
	    
	    
	    // is v < w ?
	    private static boolean less(Comparable v, Comparable w) {
	        return v.compareTo(w) < 0;
	    }

	    // is v < w ?
	    private static boolean less(Comparator comparator, Object v, Object w) {
	        return comparator.compare(v, w) < 0;
	    }
	        
	        
	    // exchange a[i] and a[j]
	    private static void exch(Object[] a, int i, int j) {
	        Object swap = a[i];
	        a[i] = a[j];
	        a[j] = swap;
	    }


}