package test.data_structures;

import org.junit.Before; 

import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;

/**
 * @author dc.gonzalesp
 *
 */
public class TestListaEncadenada extends TestCase
{

	private ListaEncadenada<Integer> escenario;
	
	
	
	private void setupEscenario1( )
	{
		escenario = new ListaEncadenada<Integer>();

	}

	public void testAgregar()
	{
		setupEscenario1();
		for (int hl =0; hl< 12; hl++)
		{
			escenario.add(hl);

		}
		assertEquals("no se agregaron todos los elementos", 12, escenario.getSize());
	}


	public void testdarElemento()

	{
		setupEscenario1();
		for (int hl =0; hl< 12; hl++)
		{

			escenario.add(hl);

		}
		assertEquals("no se agregaron todos los elementos", 12, escenario.getSize() );
		try {
			assertEquals("no es el elemento correcto",5, escenario.getElement(6).intValue());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public void testDarElementoActual()

	{
		setupEscenario1();
		for (int hl =0; hl< 12; hl++)
		{
			
			escenario.add(hl);
		}
		try {
			assertEquals( "no se obtuvo el actual", 0, escenario.getCurrentElement().intValue());
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void testEliminar(){
		setupEscenario1();
		for (int hl =0; hl< 12; hl++)
		{
			
			escenario.add(hl);
		}
		
		try {
			escenario.delete(5);
			
			assertNull( "no se eliminó el elemento", escenario.getElement(5).intValue());
		} 
		catch (Exception e1) {
			// TODO Auto-generated catch block
			
		}

	}
	
}

